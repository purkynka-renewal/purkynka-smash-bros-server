const socketio        = require("socket.io");
const express         = require('express');
const Server          = require('http').Server;
const bodyParser      = require('body-parser');
const CONFIG          = require("./config.json");

////
// Server
////
const sessions = {};

const app = express();
const server = Server(app);
const io = socketio(server, {
  path: "/socket",
  serveClient: false,
  transports: ["websocket"],
});

app.use(express.static("client/public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.post("/session", (_, res) => {
  // Don't let any more connections if the id length has been filled
  if (Object.keys(sessions).length == 10**CONFIG.idlength) {
    return res.status(503).json({ error: "server-full" });
  }

  // Find id
  let id;
  do {
    id = Math.floor(Math.random() * 10**CONFIG.idlength).toString();
    id = CONFIG.idprefix + id.padStart(CONFIG.idlength, 0);
  } while (id in sessions);

  // Create session
  const session = sessions[id] = {
    ready: false,
    host: null,
    players: {},
    sockets: {},
    activityTimeout: null,
    tick: 0,

    broadcast(...data) {
      Object.values(session.sockets).forEach(socket => {
        socket.emit(...data);
      });
    },

    syncInterval: setInterval(() => {
      session.tick = ++session.tick % CONFIG.syncRate;
      if (session && session.ready) session.updatePlayerInfo(session.tick == 0);
    }, 1000 / CONFIG.syncRate),

    updatePlayerInfo(full=true) {
      session.broadcast("gameinfo", {
        ready: session.ready,
        players: Object.entries(session.players).map(([id, player]) => {
          return [id, full ? player : {
            playerID: player.playerID,
            velocity: player.velocity,
            controls: player.controls,
            params: player.params,
            state: player.state,
            info: player.info
          }];
        }).reduce((a,i)=>((a[i[0]] = i[1]) && a), {}),
      });
    },

    refresh() {
      if (this.activityTimeout) clearTimeout(this.activityTimeout);
      this.activityTimeout = setTimeout(async () => {
        if (this.syncInterval) clearTimeout(this.syncInterval);
        delete sessions[id];

        for (const socket of Object.values(this.sockets)){
          socket.emit("fail", "session-timeout");
          socket.disconnect();
        }
      }, CONFIG.sessionTimeout);
    }
  };
  session.refresh();

  res.json({ id });
});


io.on("connection", (socket) => {
  const { nickname, sessionID } = socket.handshake.query;
  const session = sessions[sessionID];
  // Disconnect invalid sessions
  if (!session) {
    socket.emit("fail", "session-unavailable");
    return socket.disconnect();
  }
  // Prevent joing after the game started
  if (session.ready) {
    socket.emit("fail", "session-running");
    return socket.disconnect();
  }
  // Get id
  let playerID;
  for (let i = 1; i <= CONFIG.playerLimit[1]; i++) {
    if (i in session.players) continue;
    playerID = i;
    break;
  }
  // Session full
  if (!playerID) {
    socket.emit("fail", "session-full");
    return socket.disconnect();
  }
  // Check if valid
  if (!nickname) {
    socket.emit("fail", "invalid-data");
    return socket.disconnect();
  }

  // Add player
  const player = socket.player = session.players[playerID] = {
    playerID, nickname,
    isHost: !session.host, // automatically set to host if none
    // color: "hsl("+(playerID / CONFIG.playerLimit[1] * 360)+", 75%, 60%)",
    selectedChar: null,
    position: [0, 0],
    velocity: [0, 0],
    params: {},
    state: {
      health: 0,
      model: "default",
    },
    controls: {
      jump: false,
      smash: false,
      right: false,
      left: false,
    },
  };
  session.sockets[playerID] = socket;
  // Choose host
  if (player.isHost) session.host = player;

  // Send session data
  socket.emit("gameinfo", {
    playerID,
    playerLimit: CONFIG.playerLimit,
    syncRate: CONFIG.syncRate,
    players: session.players,
  });

  // New player Event
  session.updatePlayerInfo();

  socket
    /// on selectedChar
    .on("selectedChar", (charName) => {
      if (typeof charName != "string") return;
      player.selectedChar = charName;
      // Start game if all ready
      const players = Object.values(session.players);
      if (players.length >= CONFIG.playerLimit[0] &&
          players.every(p => p.selectedChar)) {
        session.ready = true;
      }
      session.updatePlayerInfo();
    })
    /// on controlsUpdate
    .on("controlsUpdate", (controls) => {
      if (!session.ready) return;
      for (const [key, value] of Object.entries(controls)){
        if (key in player.controls && typeof value == "boolean") {
          player.controls[key] = value;
        }
      }
    })
    /// on gameData
    .on("gameData", (gameData) => {
      if (!session.ready || !player.isHost) return;
      for (const [playerID, playerData] of Object.entries(gameData.players)){
        if (!(playerID in session.players)) continue;
        Object.assign(session.players[playerID], playerData);
      }
    })
    /// on DISCONNECT
    .on("disconnect", () => {

      delete session.players[playerID];
      delete session.sockets[playerID];

      // If session empty
      if (!Object.keys(session.players).length) {
        delete sessions[sessionID];
      } else {
        // Choose new host if needed
        if (player.isHost) {
          const host = Object.values(session.players)[0];
          session.host = host;
          host.isHost = true;
        }
        // Removed player Event
        session.updatePlayerInfo();
      }
    });
});

server.listen(CONFIG.port, CONFIG.host, ()=>{
  console.log("Listening on http://"+CONFIG.host+":"+CONFIG.port);
});
